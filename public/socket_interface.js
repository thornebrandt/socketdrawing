// meant to be a bridge between a mobile web and a "presenter" application.
// assigns roles to affect things on the screen. 

const socketServer = config.socketServer || "https://localhost:8080";
let debug = false;
let socket
let this_client_id;
let this_client_index = 0;
let this_role_description = "Waiting for Role";
let el = {};
let clients = {};
let identity;
let ctx; //canvas context. 
let consoleElement;
let mouse = {
    x: 0,
    y: 0,
    normalized_x: 0,
    normalized_y: 0,
}
let painting = false;
let buttonsSetup = false; //this is to prevent multiple event listeners.
let gamePadSetup = false;
let interfaceType = "none";

//used for gamepad multi-touch dragging to different buttons. 
let lastTouchElements = {};

const searchParams = new URLSearchParams(window.location.search);
if (searchParams.has('user') && searchParams.has('a')) {
    const username = searchParams.get('user');
    const userID = searchParams.get('u');
    const avatarID = searchParams.get('a');
    localStorage.setItem('username', username);
    localStorage.setItem('userID', userID);
    localStorage.setItem('avatarID', avatarID);
}

let this_client_username = localStorage.getItem('username');
let this_client_avatarID = localStorage.getItem('avatarID');
let this_client_userID = localStorage.getItem('userID');

console.log("found avatarID from query string: " + this_client_avatarID);

window.onload = async () => {
    identity = document.querySelector("body").getAttribute("id");
    el.canvas = document.querySelector("#canvas");
    el.container = document.querySelector("#canvas_container");
    el.console = document.querySelector("#console");
    el.btn_new_role = document.querySelector("#new_role");
    el.btn_hide_ui = document.querySelector("#hide_ui");

    el.interface = document.querySelector("#interface");
    el.triggers = [];

    el.all_interface_directions = document.querySelectorAll(".interface_directions");
    el.instruction_image = document.querySelector("#instruction_image_container");

    //trigger interface
    el.trigger_wrapper = document.querySelector("#trigger_wrapper");
    el.trigger_directions = document.querySelector("#trigger_directions");
    el.triggers.push(document.querySelector("#trigger1"));
    el.triggers.push(document.querySelector("#trigger2"));
    el.triggers.push(document.querySelector("#trigger3"));
    el.triggers.push(document.querySelector("#trigger4"));

    //gamepad interface
    el.gamepad_wrapper = document.querySelector("#gamepad_wrapper");
    el.gamepad_directions = document.querySelector("#gamepad_directions");

    //primary buttons. 
    el.dpad_up = document.querySelector("#dpad_up");
    el.dpad_right = document.querySelector("#dpad_right");
    el.dpad_down = document.querySelector("#dpad_down");
    el.dpad_left = document.querySelector("#dpad_left");

    //secondary buttons ( diagonal )
    el.dpad_up_right = document.querySelector("#dpad_up_right");

    el.gamepad_a_button = document.querySelector("#gamepad_button_a");
    el.gamepad_b_button = document.querySelector("#gamepad_button_b");
    el.a_button_label = document.querySelector("#gamepad_a_label");
    el.b_button_label = document.querySelector("#gamepad_b_label");

    el.ui = document.querySelector("#ui");
    el.role_description = document.querySelector("#role_description");
    el.debug = document.querySelector("#debug"); //on screen debug information. ( not debug = true ) 

    setupConsole();
    setupInterface();
    hideButtons();
    hideAllInterfaces();
    hideInstructionImage(); //will show again once socket is connected. 
    resizeCanvas();
    initSocketConnection();
};

window.onresize = () => {
    resizeCanvas();
}

const showInstructionImage = () => {
    el.instruction_image.style.display = "block";
}

const hideInstructionImage = () => {
    el.instruction_image.style.display = "none";
}

const setupButtons = () => {
    //this might need to be done after role is assigned.
    if (!buttonsSetup) {
        for (let i = 0; i < el.triggers.length; i++) {
            el.triggers[i].addEventListener("click", (e) => {
                e.preventDefault();
                socket.emit("trigger", {
                    id: this_client_id,
                    username: this_client_username,
                    avatarID: this_client_avatarID,
                    userID: this_client_userID,
                    x: mouse.normalized_x,
                    y: mouse.normalized_y,
                    triggerID: i + 1  //will use when there are multiple triggers. 
                });
            });

            el.triggers[i].addEventListener("touchstart", (e) => {
                e.preventDefault();
                socket.emit("trigger", {
                    id: this_client_id,
                    username: this_client_username,
                    avatarID: this_client_avatarID,
                    userID: this_client_userID,
                    x: mouse.normalized_x,
                    y: mouse.normalized_y,
                    triggerID: i + 1
                });
            });

        };
        buttonsSetup = true;
    }
}

const cleanupTouches = (e) => {
    //for gamepad
    for (let i = 0; i < e.changedTouches.length; i++) {
        const touch = e.changedTouches[i];
        const touchElement = lastTouchElements[touch.identifier];
        if (touchElement != null) {
            touchElement.dispatchEvent(new Event("mouseup"));
        }
        //remove touch from lastTouchElements
        delete lastTouchElements[touch.identifier];
    }
};


const checkForOtherFingers = (lastTouchElements, touchElement) => {
    for (let i = 0; i < Object.keys(lastTouchElements).length; i++) {
        if (lastTouchElements[Object.keys(lastTouchElements)[i]] == touchElement) {
            return true;
        }
    }
    return false;
}

const handleTouches = (e) => {
    const touches = e.touches;
    for (let i = 0; i < touches.length; i++) {
        const touch = touches[i];
        const actualTouchedElement = document.elementFromPoint(touch.clientX, touch.clientY);
        let touchElement = null;
        if (actualTouchedElement) {
            touchElement = actualTouchedElement.closest("a");
        }
        if (touchElement) {
            el.debug.innerHTML = "closest element: " + touchElement.id;
            const lastTouchElement = lastTouchElements[touch.identifier];
            if (touchElement != lastTouchElement) {
                touchElement.dispatchEvent(new Event("mousedown"));  //experimental.

                if (lastTouchElement) {
                    const otherFingers = checkForOtherFingers(lastTouchElements, touchElement);
                    if (!otherFingers) {
                        lastTouchElement.dispatchEvent(new Event("mouseup"));
                        delete lastTouchElements[touch.identifier];
                    }
                }
                lastTouchElements[touch.identifier] = touchElement;
            }
        } else {
            el.debug.innerHTML = "cleaning up: " + actualTouchedElement.id;
            cleanupTouches(e);
        }
    }
}

const setupGamePad = () => {
    if (!gamePadSetup) {
        gamePadSetup = true;

        //multi touch events for gamepad interface.
        el.gamepad_wrapper.addEventListener("touchstart", (e) => {
            e.preventDefault();
            handleTouches(e);
        }, { passive: false });

        el.gamepad_wrapper.addEventListener("touchend", (e) => {
            e.preventDefault();
            cleanupTouches(e);
        }, { passive: false });

        el.gamepad_wrapper.addEventListener("touchcancel", (e) => {
            e.preventDefault();
            cleanupTouches(e);
        }, { passive: false });

        el.gamepad_wrapper.addEventListener("touchmove", (e) => {
            e.preventDefault();
            handleTouches(e);
        }, { passive: false });



        //----up
        el.dpad_up.addEventListener("mousedown", (e) => {
            e.preventDefault();
            socket.emit("dpadOn", {
                id: this_client_id,
                triggerID: 1
            });
        });

        el.dpad_up.addEventListener("mouseup", (e) => {
            e.preventDefault();
            socket.emit("dpadOff", {
                id: this_client_id,
                triggerID: 1
            });
        });

        //--right
        el.dpad_right.addEventListener("mousedown", (e) => {
            e.preventDefault();
            socket.emit("dpadOn", {
                id: this_client_id,
                triggerID: 2
            });
        });

        el.dpad_right.addEventListener("mouseup", (e) => {
            e.preventDefault();
            socket.emit("dpadOff", {
                id: this_client_id,
                triggerID: 2
            });
        });

        //----down
        el.dpad_down.addEventListener("mousedown", (e) => {
            e.preventDefault();
            socket.emit("dpadOn", {
                id: this_client_id,
                triggerID: 3
            });
        });

        el.dpad_down.addEventListener("mouseup", (e) => {
            e.preventDefault();
            socket.emit("dpadOff", {
                id: this_client_id,
                triggerID: 3
            });
        });

        //---left
        el.dpad_left.addEventListener("mousedown", (e) => {
            e.preventDefault();
            socket.emit("dpadOn", {
                id: this_client_id,
                triggerID: 4
            });
        });


        el.dpad_left.addEventListener("mouseup", (e) => {
            e.preventDefault();
            socket.emit("dpadOff", {
                id: this_client_id,
                triggerID: 4
            });
        });



        //---a button
        el.gamepad_a_button.addEventListener("mousedown", (e) => {
            e.preventDefault();
            socket.emit("buttonOn", {
                id: this_client_id,
                username: this_client_username,
                avatarID: this_client_avatarID,
                userID: this_client_userID,
                triggerID: 1
            });
        });

        el.gamepad_a_button.addEventListener("mouseup", (e) => {
            e.preventDefault();
            socket.emit("buttonOff", {
                id: this_client_id,
                triggerID: 1
            });
        });


        //---b button
        el.gamepad_b_button.addEventListener("mousedown", (e) => {
            e.preventDefault();
            socket.emit("buttonOn", {
                id: this_client_id,
                username: this_client_username,
                avatarID: this_client_avatarID,
                triggerID: 2
            });
        });

        el.gamepad_b_button.addEventListener("mouseup", (e) => {
            e.preventDefault();
            socket.emit("buttonOff", {
                id: this_client_id,
                triggerID: 2
            });
        });
    }
}

const hideButtons = () => {
    for (let i = 0; i < el.triggers.length; i++) {
        el.triggers[i].style.display = "none";
    }
}

const hideAllDirections = () => {
    el.all_interface_directions.forEach(el => {
        el.style.display.none;
    });
}

const hideAllInterfaces = () => {
    el.gamepad_wrapper.style.display = "none";
    el.trigger_wrapper.style.display = "none";
    el.interface.style.display = "none";
    hideButtons();
}

function displayInterface(user) {
    //TODO - eventually will be a loop. 
    hideAllDirections();
    hideAllInterfaces();
    if (user.roleObj.interfaceType == "gamepad") {
        this.displayGamePadInterface(user);
    } else {
        this.displayTriggerInterface(user);
    }
}

function displayTriggerInterface(user) {
    if (user.roleObj != null) {
        el.trigger_directions.style.display = "inline-block";
        //TODO - loop through triggers.
        if (user.roleObj.trigger1Display && user.roleObj.trigger1) {
            interfaceType = "trigger";
            el.interface.style.display = "flex";
            el.trigger_wrapper.style.display = "flex";
            el.triggers[0].style.display = "flex";
            el.triggers[0].innerHTML = user.roleObj.trigger1Display;
            // ( event listeners should already be setup from right after socket connection. ) 
        } else {
            interfaceType = "none";
            el.interface.style.display = "none";
            el.triggers[0].style.display = "none"; //probably don't need this.
        }
    }
}

function displayGamePadInterface(user) {
    if (user.roleObj != null) {
        el.gamepad_directions.style.display = "inline-block";
        interfaceType = "gamepad";
        el.interface.style.display = "flex";
        el.gamepad_wrapper.style.display = "flex";
        el.gamepad_directions.style.display = "block";
        if (user.roleObj.trigger1Display && user.roleObj.trigger1) {
            el.a_button_label.innerHTML = user.roleObj.trigger1Display;
        }

        if (user.roleObj.trigger2 && user.roleObj.trigger2Display) {
            el.b_button_label.innerHTML = user.roleObj.trigger2Display;
        }
    }
}

const resizeCanvas = () => {
    el.canvas.width = el.canvas.offsetWidth;
    el.canvas.height = el.canvas.offsetHeight;
};

const addToConsole = (_string) => {
    el.console.innerHTML += "<br>" + _string;
}

const setupConsole = () => {
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
    if (params.debug) {
        debug = true;
        el.debug.style.display = "block";
    } else {
        el.debug.style.display = "none";
    }
    if (debug && "console" in window) {
        methods = [
            "log", "assert", "clear", "count",
            "debug", "dir", "dirxml", "error",
            "exception", "group", "groupCollapsed",
            "groupEnd", "info", "profile", "profileEnd",
            "table", "time", "timeEnd", "timeStamp",
            "trace", "warn"
        ];

        generateNewMethod = function (oldCallback, methodName) {
            return function () {
                var args;
                addToConsole(methodName + ":" + arguments[0]);
                args = Array.prototype.slice.call(arguments, 0);
                Function.prototype.apply.call(oldCallback, console, arguments);
            };
        };

        for (i = 0, j = methods.length; i < j; i++) {
            cur = methods[i];
            if (cur in console) {
                old = console[cur];
                console[cur] = generateNewMethod(old, cur);
            }
        }
    }
}

// function addIdentityToClients(user) {
//     for (let i = 0; i < Object.keys(clients).length; i++) {
//         if (Object.keys(clients)[i] == user.id) {
//             clients[user.id].identity = user.identity;
//         }
//     };
// }

function initSocketConnection() {
    const authObject = {};
    if (this_client_username) {
        authObject.username = this_client_username;
    }
    if (this_client_avatarID) {
        authObject.avatarID = this_client_avatarID;
    }
    if (this_client_userID) {
        authObject.userID = this_client_userID;
    }
    console.log("attempting socket connection");
    socket = io(socketServer, { secure: true, reconnectionAttempts: 3, auth: authObject });
    socket.on("connect", () => {
        console.log("socket.io connected to " + socketServer);
        setupButtons(); //sets up event listeners. for buttons will possibly duplicate.
        setupGamePad();
    });

    socket.on("connect_failed", (e) => {
        console.log("connect_failed: ", e);
        handleNoPresenter(); //TODO - make more verbose connection handler. 
    });

    socket.on("error", (e) => {
        console.log("error: ", e);
        handleNoPresenter(); //TODO - make more verbose connection handler. 
    });

    socket.on("disconnect", (reason) => {
        console.log("Socket disconnected:", reason);
        handleNoPresenter(); //TODO - make more verbose connection handler. 
    });

    socket.on("connect_error", (err) => {
        console.error("Connection Error:", err.message);
        handleNoPresenter();
    });

    //this assigns the id to the payload obj. 
    socket.on("introduction", (payload) => {
        console.log("first introduced, obtaining socket id: ", payload);
        //check for false negatives after presenter connects. 

        // Noo--- you are providing these, not listening for these!! 
        // this_client_id = payload.id;
        // this_client_avatarID = payload.avatarID;
        // this_client_userID = payload.userID;

        if (!payload.presenter_connected) {
            handleNoPresenter();
        }
    });

    socket.on("role_assigned_handler", (payload) => {
        //after introduction to presenter.
        displayRole(payload);
    });

    socket.on("presenter_connected", (payload) => {
        //presenter connected after client. 

        socket.emit("client_connected", {
            id: this_client_id,
            username: this_client_username,
            avatarID: this_client_avatarID,
            userID: this_client_userID,
        });
        handlePresenterConnected();
    });

    socket.on("presenter_disconnected", (payload) => {
        handleNoPresenter();
    });

    //TODO - on presenter disconnected, hideUI and add messaging. 

    socket.on("state_changed_handler", (payload) => {
        //TODO - check that we can swap to a new state. 
        const users = payload;
        users.forEach((user) => {
            if (user.id == this_client_id) {
                displayRole(user);
            }
        });
    });
}

function displayRole(user) {
    //TODO - split this up into just using roleDescription, not setting it. 
    this_role_description = user.roleObj.description;
    if (!this_role_description) {
        console.log("no role description: ", user);
    };
    el.role_description.innerHTML = this_role_description;
    el.btn_new_role.style.display = "inline-block";
    el.role_description.classList.remove("warning");
    el.ui.style.display = 'block';
    showInstructionImage();
    displayInterface(user);
}

function handleNoPresenter() {
    this_role_description = "Server is resetting. Hang tight!";
    el.role_description.innerHTML = this_role_description;
    el.btn_new_role.style.display = "none";
    el.role_description.classList.add("warning");
    hideAllInterfaces();
    hideInstructionImage();
    hideButtons();
}

function handlePresenterConnected() {
    el.role_description.innerHTML = "Finding your role.";
    el.role_description.classList.remove("warning");
    hideButtons();
    showInstructionImage();
}

function requestNewRole() {
    console.log("requesting new role: avatarID" + this_client_avatarID);
    socket.emit("new_role", {
        username: this_client_username,
        avatarID: this_client_avatarID,
        userID: this_client_userID,
        id: this_client_id
    });
}

function setupInterface() {
    ctx = el.canvas.getContext('2d');
    ctx.lineWidth = 20;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';


    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimaitonFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    el.canvas.addEventListener('mousedown', (e) => {
        e.preventDefault();
        hideInstructionImage(); //we have moved. 
        console.log("mousedown on canvas: " + e.pageY);
        mouse = getMousePosition(e);
        painting = true;
    }, false);

    el.canvas.addEventListener('mouseup', () => {
        ctx.closePath;
        painting = false;
        socket.emit("message", {
            message: "mouseUp",
            x: mouse.normalized_x,
            y: mouse.normalized_y,
            id: this_client_id,
        });
    }, false);

    el.canvas.addEventListener('mousemove', (e) => {
        if (painting) {
            onMousePaint(e);
        }
    });

    el.canvas.addEventListener("touchstart", (e) => {
        hideInstructionImage(); //we have moved. 
        let touch = e.touches[0];
        mouse = getMousePosition(touch);
        painting = true;
    }, false);

    el.canvas.addEventListener("touchend", (e) => {
        let touch = e.touches[0];
        painting = false;
        socket.emit("message", {
            message: "mouseUp",
            username: this_client_username,
            avatarID: this_client_avatarID,
            userID: this_client_userID,
            x: mouse.normalized_x,
            y: mouse.normalized_y,
            id: this_client_id,
        });
    }, false);

    el.canvas.addEventListener("touchmove", (e) => {
        let touch = e.touches[0];
        if (painting) {
            onMousePaint(touch);
        }
        //el.canvas.dispatchEvent(mouseEvent); //not sure why this didn't break anything before.
    }, false);

    document.body.addEventListener("touchstart", function (e) {
        if (e.target == el.canvas) {
            e.preventDefault();
        }
    }, false);
    document.body.addEventListener("touchend", function (e) {
        if (e.target == el.canvas) {
            e.preventDefault();
        }
    }, false);



    document.body.addEventListener("touchmove", function (e) {
        if (e.target == el.canvas) {
            e.preventDefault();
        }
    }, false);

    const onMousePaint = (e) => {
        if (interfaceType == "none") {
            //experimental.
            el.interface.style.display = "none";
        }

        mouse = getMousePosition(e);
        socket.emit("xy", {
            username: this_client_username,
            avatarID: this_client_avatarID,
            userID: this_client_userID,
            x: mouse.normalized_x,
            y: mouse.normalized_y,
            id: this_client_id
        });
        onPaint(mouse.x, mouse.y);
    };


    el.btn_new_role.addEventListener("click", (e) => {
        e.preventDefault();
        //request new role 
        requestNewRole();
    });

    el.btn_hide_ui.addEventListener("click", (e) => {
        e.preventDefault();
        //TODO - refresh to get UI back. 
        el.ui.style.display = 'none';
    });

    //should ask for a new role after the phone wakes up from sleep. 
    document.addEventListener('visibilitychange', function () {
        if (document.visibilityState === 'visible') {
            //perhaps refresh. 
            requestNewRole();
        }
    });
}

function clamp01(value) {
    return Math.max(0, Math.min(1, value));
}

function getMousePosition(e) {
    const rect = canvas.getBoundingClientRect();
    mouse.x = e.clientX - rect.left;
    mouse.y = e.clientY - rect.top;
    mouse.normalized_x = clamp01(mouse.x / rect.width);
    mouse.normalized_y = clamp01(mouse.y / rect.height);
    return mouse;
}

function getTouchPosition(e) {
    mouse.x = e.touches[0].pageX - el.canvas.offsetLeft;
    mouse.y = e.touches[0].pageY - el.canvas.offsetTop;
    mouse.normalized_x = mouse.x / el.canvas.width;
    mouse.normalized_y = mouse.y / el.canvas.height;
    return mouse;
}

function clearCanvas() {
    ctx.closePath;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.beginPath();
}

function onPaint(x, y) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.beginPath();
    ctx.moveTo(0, y);
    ctx.lineTo(canvas.width, y);
    ctx.moveTo(x, 0);
    ctx.lineTo(x, canvas.height);
    ctx.strokeStyle = "#fff";
    ctx.stroke();
    ctx.closePath();
    // ctx.lineTo(x, y);
    // ctx.stroke();
}

function addClient(_id) {
    clients[_id] = {};
    clients[_id].id = _id;
}

