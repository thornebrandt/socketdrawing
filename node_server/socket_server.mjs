import fs from 'fs';
import { setupDiscordClient, setupDiscordCommands, DiscordStateChangeHandler, SendDischordMessage } from './discord_client.mjs';
import express from 'express';
import config from './config.js';
import https from 'https';
import { Server } from 'socket.io';

import state from './state.js';

state.app = express();

state.presenter_connected = false; //unity or touch designer host. 
state.current_presenter = null;

//set ssl options. 
let ssl_options;
if (config.local) {
    ssl_options = {
        key: fs.readFileSync('certs/server.key'),
        cert: fs.readFileSync('certs/server.crt')
    }
} else {
    ssl_options = {
        key: fs.readFileSync(config.certPath + 'key.pem'),
        cert: fs.readFileSync(config.certPath + 'cert.pem'),
        ca: fs.readFileSync(config.certPath + 'chain.pem'),
    }
}


async function main() {
    await setupHttpsServer();
    setupSocketServer();
    setupDiscordClient();
    setupDiscordCommands();
}

async function setupHttpsServer() {
    state.port = process.env.PORT || 8081;
    var serverHttps = https.createServer(ssl_options, state.app).listen(state.port, () => {
        console.log("listening on " + state.port);
    });
    state.io = new Server({
        cors: {
            origin: config.cors_origin,
            credentials: true,
        },
        allowEIO3: true
    }).listen(serverHttps);
}

function parseData(data) {
    //mostly for parsing the discord. 
    if (data.avatarID && data.userID) {
        data.avatar = `https://cdn.discordapp.com/avatars/${data.userID}/${data.avatarID}.png?size=512`;
    }
    if (data.username) {
        data.user = data.username;
    }
    return data;
}

function setupSocketServer() {
    console.log("setting up socket server");

    state.io.on("connection", (thisClient) => {
        let isPresenter = !isEmpty(thisClient.handshake.auth.presenter);
        if (isPresenter) {
            thisClient.presenter = true;
            console.log("Unity host Presenter connected.");
            state.presenter_connected = true;
            state.current_presenter = thisClient.id;
            state.presenters.push(thisClient.id);
            // introduction will happen for browser clients after host assigns a roll and clientIndex.
            thisClient.emit(
                "introduction",
                {
                    presenter: true,
                    roleObj: {
                        name: "presenter",
                        description: "Unity host"
                    },
                    id: thisClient.id,
                    message: "Discord connected: " + state.discord_connected,
                    clientIndex: -1
                }
            );

            //tell everyone else that a new presenter has arrived. 
            state.io.emit(
                "presenter_connected",
                {
                    id: thisClient.id
                }
            );

            thisClient.on('disconnect', () => {
                state.presenter_connected = false;
                state.current_presenter = null;
                state.presenters = state.presenters.filter(presenter_id => presenter_id != thisClient.id);
                if (state.presenters.length == 0) {
                    thisClient.broadcast.emit("presenter_disconnected", {
                        message: "Presenter has disconnected."
                    });
                } else {
                    //get the last presenter in the queue. 
                    state.current_presenter = state.presenters[state.presenters.length - 1];
                }
            });


            //presenter has sent a new role to data.id
            thisClient.on("role_assigned", (user) => {
                user = parseData(user); //experimental. 
                thisClient.roleObj = user.roleObj;
                state.io.to(user.id).emit(
                    "role_assigned_handler",
                    user
                );
            });

            //presenter calls this on new part. 
            thisClient.on("state_changed", (presenterState) => {
                DiscordStateChangeHandler(presenterState);
                //only need to send socket users users to the clients. 
                //saving chatRoles for discord. 
                thisClient.broadcast.emit("state_changed_handler", presenterState.users);
            });
        } else {
            //this block is meant for a webclient : thisClient.   It sends to the presenter ( unity ) 
            //FYI - io.to(bogus presenter) is safe to run in case it is discconected. 

            state.clients.push(thisClient.id);  //what are we using clients for?

            thisClient.emit("introduction", {
                id: thisClient.id,
                current_presenter: state.current_presenter,
                presenter_connected: state.presenter_connected,
            });

            if (state.presenter_connected) {
                //this sends the client to the presenter if.

                //let isPresenter = !isEmpty(thisClient.handshake.auth.presenter);  //not doing anything.  

                state.io.to(state.current_presenter).emit(
                    "introduction",
                    {
                        roleObj: {
                            name: "new",
                            description: "new browser client"
                        },
                        id: thisClient.id,
                        avatar: thisClient.handshake.auth.avatar,
                        user: thisClient.handshake.auth.user,
                    }
                );
            }

            thisClient.on('disconnect', () => {
                state.io.to(state.current_presenter).emit("client_disconnected", {
                    message: "Client has disconnected.",
                    id: thisClient.id
                });
            });

            thisClient.on('client_connected', (payload) => {
                
                payload.avatarID = thisClient.handshake.auth.avatarID;
                payload.userID = thisClient.handshake.auth.userID;
                payload.username = thisClient.handshake.auth.username;
                
                payload = parseData(payload);
                state.io.to(state.current_presenter).emit(
                    "introduction",
                    {
                        roleObj: {
                            name: "new",
                            description: "new browser client"
                        },
                        id: thisClient.id,
                        ...payload
                    }
                );
            });

            thisClient.on('presenter_connected', (data) => {
                state.presenter_connected = true;
                state.current_presenter = data.id;
                //data = parseData(data); //this is the presenter's data. 
                state.io.to(state.current_presenter).emit(
                    "introduction",
                    {
                        roleObj: {
                            name: "new",
                            description: "new browser client"
                        },
                        id: thisClient.id,
                        ...data
                    }
                );
            });

            thisClient.on('presenter_disconnected', () => {
                state.presenter_connected = false;
                state.current_presenter = false;
                state.chatRoles = [];
                state.current_chat_role_index = 0;
                thisClient.roleObj = null;
                //TODO - some kind of destruction of all clients? ( probbaly needs to happen on client side ) 
            });

            // data for the below events should include client id, role, and any other relevant data.
            thisClient.on("xy", (data) => {
                data = parseData(data); //for avatar - SLOW remove if this slows things down. 

                state.io.to(state.current_presenter).emit(
                    "xy_handler", {
                    id: thisClient.id,
                    ...data
                });
            });

            //distinct from discord_trigger.
            thisClient.on("trigger", (data) => {
                // data also send x and y information ) 
                data = parseData(data); //hopefully sending avatar stuff. 
                state.io.to(state.current_presenter).emit(
                    "trigger_handler", {
                    id: thisClient.id,
                    ...data
                });
            });

            //not implemented yet - don't want to complicate states yet
            thisClient.on("triggerOff", (data) => {
                data = parseData(data);
                state.io.to(state.current_presenter).emit(
                    "triggerOff_handler", {
                    id: thisClient.id,
                    ...data
                });
            });

            //game pad buttons. 
            thisClient.on("buttonOn", (data) => {
                data = parseData(data);
                state.io.to(state.current_presenter).emit(
                    "buttonOn_handler", {
                    id: thisClient.id,
                    ...data
                });
            });

            thisClient.on("buttonOff", (data) => {
                data = parseData(data);
                state.io.to(state.current_presenter).emit(
                    "buttonOff_handler", {
                    id: thisClient.id,
                    ...data
                });
            });

            //game pad dpad. I am separating these in case they send better simultaneously.
            //( trigger is more generic. )
            thisClient.on("dpadOn", (data) => {
                data = parseData(data);
                state.io.to(state.current_presenter).emit(
                    "dpadOn_handler", {
                    id: thisClient.id,
                    ...data
                });
            });

            thisClient.on("dpadOff", (data) => {
                state.io.to(state.current_presenter).emit(
                    "dpadOff_handler", {
                    id: thisClient.id,
                    ...data
                });
            });

            thisClient.on("new_role", (data) => {
                //client has requested a new role. 
                data = parseData(data);
                state.io.to(state.current_presenter).emit(
                    "new_role_handler", {
                    id: thisClient.id,
                    ...data
                });
            });
        }

        //both presenter and client. 
        thisClient.on("message", (payload) => {
            // this is a catch all for messages that are not specific to a role.
            // backwards compatible. 
            payload = parseData(payload); //just in case. 
            thisClient.broadcast.emit("message",
                {
                    client_id: thisClient.id,
                    id: thisClient.id,
                    presenter: thisClient.presenter,
                    ...payload
                }
            );
        });

        thisClient.on("error", (err) => {
            console.log("there was an error on the connection!", err);
        });
    });
}

function isEmpty(value) {
    return (value == null || value == undefined || value == "undefined" ||
        value == "null" || value == "false" || value == false || (typeof value === "string" && value.trim().length === 0));
}

main();