//using this here for referece. 

const fs = require('fs');
const express = require('express');
const app = express();
const multer = require('multer');
const path = require('path');
const config = require('./config.js');
const cors = require('cors');



let ssl_options;

if (config.local) {
    ssl_options = {
        key: fs.readFileSync('certs/server.key'),
        cert: fs.readFileSync('certs/server.crt')
    }
} else {
    ssl_options = {
        key: fs.readFileSync(config.certPath + 'key.pem'),
        cert: fs.readFileSync(config.certPath + 'cert.pem'),
        ca: fs.readFileSync(config.certPath + 'chain.pem'),
    }
}


var port;
var io;

let clientIndex = 0;

let clients = {};


const webcam_storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/webcam');
    },
    filename: (req, file, cb) => {
        let imageName = 'webcam' + Date.now() + path.extname(file.originalname);
        console.log('uploading_webcam_storage', imageName);
        cb(null, imageName);
    }
});

const processed_image_storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/processed_images');
    },
    filename: (req, file, cb) => {
        //imagename is handled by python. 
        cb(null, file.originalname);
    }
});


const webcam_upload = multer({ storage: webcam_storage });
const processed_image_upload = multer({ storage: processed_image_storage });

app.use(express.json({ limit: '50mb' }));
app.use(express.static("public"));
app.use(cors({
    origin: 'https://experiments.thornebrandt.com'
}));

app.locals.config = config;

app.post("/webcam", webcam_upload.single('image'), (req, res) => {
    //req.body is the image
    console.log("uploading webcam?", res.status, "id: " + req.body.id);
    const responsePayload = {
        message: 'webcam uploaded',
        id: req.body.id,
        file: req.file.filename
    };
    res.status(200).send(responsePayload);
});

app.post("/processed_image", processed_image_upload.single('image'), (req, res) => {
    console.log("post worked: " + req.file.filename, + "id: " + req.body.id);
    const responsePayload = {
        message: 'processed image uploaded',
        id: req.body.id,
        file: req.originalName
    }
    res.send(responsePayload);
});


async function main() {
    await setupHttpsServer();
    setupSocketServer();
}


async function setupHttpsServer() {
    var https = require('https');
    port = process.env.PORT || 8080;

    var serverHttps = https.createServer(ssl_options, app).listen(port, () => {
        console.log("listening on " + port);
    });

    io = require("socket.io")({
        cors: {
            origin: "https://experiments.thornebrandt.com",
            credentials: true,
        },
        allowEIO3: true
    }).listen(serverHttps);
}

function setupSocketServer() {
    console.log("setting up socket server");
    io.on("connection", (client) => {

        let thisClientIndex = clientIndex;
        clientIndex++;

        console.log("User " + client.id + " connected. Assigning index: " + thisClientIndex);

        clients[client.id] = {
            id: client.id,
            client_index: thisClientIndex,
        };

        client.emit(
            "introduction",
            {
                id: client.id,
                client_index: thisClientIndex,
                clients: Object.keys(clients)
            }
        );

        io.sockets.emit(
            "newUserConnected",
            {
                id: client.id,
                client_index: thisClientIndex,
                clients: Object.keys(clients),
            }
        );

        client.on("disconnect", () => {
            delete clients[client.id];
            io.sockets.emit(
                "userDisconnected",
                {
                    id: client.id,
                    client_index: thisClientIndex,
                    clients: Object.keys(clients)
                }
            );
            console.log(
                "User " + client.id + "(" + thisClientIndex + ") disconnected"
            );
        });

        client.on("connect_failed", (err) => {
            console.log("connect failed!", err);
        });

        client.on("error", (err) => {
            console.log("there was an error on the connection!", err);
        });

        //kept as an example. 
        client.on("mouseMove", (data) => {
            client.broadcast.emit(
                "onMouseMove",
                {
                    id: client.id,
                    client_index: thisClientIndex,
                    ...data
                }
            );
        });

        client.on("message", (data) => {
            console.log(thisClientIndex + ":" + data.message, data);
            io.sockets.emit(
                "onMessage",
                {
                    id: client.id,
                    client_index: thisClientIndex,
                    ...data
                }
            );
        });

        client.on("image_uploaded", (data) => {
            console.log("image_uploaded: ", data);
            if (data.client_id) {
                console.log("python (" + client.id + ") is sending image to " + data.client_id);
            }
            //this client id is gonna be the python id. 
            client.broadcast.emit(
                "onImageUpload",
                {
                    id: client.id,
                    client_index: thisClientIndex,
                    ...data
                }
            );
        });

        client.on("webcam_uploaded", (data) => {
            console.log("webcam uploaded: ", data);
            client.broadcast.emit(
                "onWebcamUploaded",
                {
                    id: client.id,
                    client_index: thisClientIndex,
                    ...data
                }
            );
        });

    });
}

main();