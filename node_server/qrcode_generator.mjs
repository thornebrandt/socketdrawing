import QRCode from 'qrcode';

// The text you want to encode in the QR code
const text = 'https://experiments.thornebrandt.com/dischord';

// Generate and save the QR code as a PNG file
QRCode.toFile('dischord_qr_code.png', text, function (err) {
    if (err) throw err;
    console.log('QR code has been generated and saved as testQRCode.png');
});