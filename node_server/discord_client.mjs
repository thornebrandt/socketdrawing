import { Client, Collection, Events, GatewayIntentBits, ActionRowBuilder, ButtonBuilder, AttachmentBuilder, ButtonStyle } from 'discord.js';
import { VerifyDiscordRequest, getRandomEmoji, DiscordRequest } from './utils.mjs';
import {
    InteractionType,
    InteractionResponseType,
    InteractionResponseFlags,
    MessageComponentTypes,
    ButtonStyleTypes,
} from 'discord-interactions';
import express from 'express';
import state from './state.js';
import config from './config.js';
import QRCode from 'qrcode';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

export function setupDiscordClient() {
    //this command is general logic for the discord bot. 
    //slash commands are not included. 

    state.discord_connected = false;

    const discordClient = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMembers, GatewayIntentBits.GuildMessages, GatewayIntentBits.GuildIntegrations, GatewayIntentBits.DirectMessages, GatewayIntentBits.MessageContent] });
    discordClient.on('ready', () => {
        console.log(`Logged in as ${discordClient.user.tag}! About to get ${config.channels[0]}`);
        state.discordChannel = discordClient.channels.cache.get(config.channels[0]);
        console.log("found channel: " + state.discordChannel);
        //do not try (!state.dischordChannel) it's always false. 

        if (state.presenter_connected) {
            state.discordChannel.send("Dischord is resetting. Presenter is already connected.");
        } else {
            state.discordChannel.send("Dischord is resetting. Waiting for the presenter to connect.");
        }

        sendAutoInstructions();
        setInterval(() => {
            sendAutoInstructions();
        }, 60 * 60 * 1000); //Attempt to send a message every hour.
    });

    discordClient.on(Events.MessageCreate, message => {
        if (message.author.bot) return;  //we don't need to listen to the bot. 
        const avatarURL = message.author.displayAvatarURL({ extension: 'png', size: 512 });
        //current method of strum. 
        state.io.to(state.current_presenter).emit(
            "discord_message",
            {
                message: message.content,
                user: message.author.username,
                avatar: avatarURL
            }
        );
    });

    discordClient.login(config.discord_app_token);
}

export function SendDischordMessage(message) {
    if (state.discordChannel) {
        state.discordChannel.send(message);
    } else {
        console.log("Attempting to send message " + message + "but no discord channel found.");
    }
}

export function DiscordStateChangeHandler(_state) {
    state.chatRoles = _state.chatRoles;
    state.current_chat_role_index = -1; //perhaps sloppy. always progresses by one. 
    state.button_dictionary = parseButtonDictionary(state.chatRoles);
    console.log("state has changed. we have " + state.chatRoles.length + " chat roles and " + Object.keys(state.button_dictionary).length + " buttons.");
    state.discordChannel.send("Presenter: Let's start a new song! Type `/dischord` to get started! ");
}

function generateRandomString(length = 8) {
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    for (let i = 0; i < length; i++) {
        result += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return result;
}

function flattenButtonTriggers(trigger, roleIndex, parentPath, buttonDictionary) {
    const randomString = generateRandomString(4);
    let sanitizedEventName = "btn";

    if (trigger.eventName) {
        sanitizedEventName = trigger.eventName
            .replace(/\s+/g, '_')
            .replace(/[^a-zA-Z0-9_]/g, '_'); //just to be safe.
    } else {
        if (trigger.display) {
            sanitizedEventName = trigger.display
                .replace(/\s+/g, '_')
                .replace(/[^a-zA-Z0-9_]/g, '');
        }
    }

    const uniqueId = sanitizedEventName + "_" + randomString;
    trigger.id = uniqueId; //smelly but...not sure another way. 
    trigger.type = trigger.type || 'trigger';

    buttonDictionary[uniqueId] = {
        ...trigger,
        parentPath //for back navigation
    };




    if (trigger.triggers && trigger.triggers.length > 0) {
        trigger.triggers.forEach((nestedTrigger) => {
            flattenButtonTriggers(nestedTrigger, roleIndex, uniqueId, buttonDictionary);
        });
    }
}

function parseButtonDictionary(chatRoles) {
    const buttonDictionary = {};

    if (chatRoles) {
        chatRoles.forEach((chatRole, roleIndex) => {
            chatRole.triggers.forEach((trigger) => {
                flattenButtonTriggers(trigger, roleIndex, '', buttonDictionary);
            });
        });
    } else {
        console.log('No chat roles found in this state.');
    }

    return buttonDictionary;
}

function getNextChatRoleIndex(state) {
    state.current_chat_role_index += 1;
    if (state.current_chat_role_index >= state.chatRoles.length) {
        state.current_chat_role_index = 0;
    }
    return state.current_chat_role_index;
}

function sendAutoInstructions() {
    // checking if presenter is connected before announcing one-self. 
    if (state.presenter_connected) {
        state.discordChannel.send("Howdy! We're setup to make music together. Join the stream and type `/dischord` to get started. \n"
            + "To use an xy pad, visit [DischordXY](https://experiments.thornebrandt.com/dischord/) \n"
            + "https://experiments.thornebrandt.com/dischord/qr/qr.png");

    }
}

function getButtonStyle(style_string) {
    switch (style_string) {
        case 'primary':
            return ButtonStyle.Primary;
        case 'secondary':
            return ButtonStyle.Secondary;
        case 'success':
            return ButtonStyle.Success;
        case 'danger':
            return ButtonStyle.Danger;
        default:
            return ButtonStyle.Primary;
    }
}

function commonButtonsRow(endEvent = "") {
    const row = new ActionRowBuilder();
    const rowButtons = [];
    //common buttons.
    const metaData = endEvent ? "|" + endEvent : "";
    //right now the metaData is an endEvent. 
    //in an effort to keep it more silent, this will remove introduced objects. 

    const rerollButton = new ButtonBuilder()
        .setCustomId('introduction' + metaData) //eventually will be a confirm/cancel, right now just re-calling introduction. 
        .setLabel('Re-Roll (Do something else)') // Button label
        .setEmoji('🔄')
        .setStyle(ButtonStyle.Danger);
    const xypadButton = new ButtonBuilder()
        .setCustomId('xypad' + metaData)
        .setLabel('Use Mobile (QR for XY-Pad)')
        .setEmoji('📱')
        .setStyle(ButtonStyle.Success);
    rowButtons.push(rerollButton);
    rowButtons.push(xypadButton);
    row.addComponents(rowButtons);
    return row;
}


export function setupDiscordCommands() {
    // hits the /interactions and creates the slash commands. 

    state.app.use(express.json({ verify: VerifyDiscordRequest(config.discord_public_key) }));


    state.app.post('/interactions', async function (req, res) {
        // Interaction type and data
        const { type, id, data, member, message } = req.body;

        if (!member) {
            console.log("there is no member: ", message);
            return;
        }
        const { user } = member;

        if (!user) {
            console.log("there is no user: ", member);
            return;
        }

        let avatarURL = null;
        let avatarID = ""; //sending for QR code. 
        let userID = "";
        if (user.avatar && user.id) {
            avatarID = user.avatar;
            userID = user.id;
            avatarURL = `https://cdn.discordapp.com/avatars/${user.id}/${user.avatar}.png?size=512`;
        }

        const username = user.username;

        if (type === InteractionType.PING) {
            return res.send({ type: InteractionResponseType.PONG });
        }

        //slash commands. 
        if (type === InteractionType.APPLICATION_COMMAND) {
            const { name } = data;



            switch (name) {
                case 'dischord':
                    if (state.presenter_connected) {
                        //allow button or QR code. 

                        const dischordButton = new ButtonBuilder()
                            .setCustomId('introduction') // Unique ID for the button
                            .setLabel('Interact With Stream') // Button label
                            .setEmoji('🎹')
                            .setStyle(ButtonStyle.Primary); // Button style ('PRIMARY', 'SECONDARY', 'SUCCESS', 'DANGER')

                        const xypadButton = new ButtonBuilder()
                            .setCustomId('xypad') // Unique ID for the button
                            .setLabel('Use Mobile (QR for XY-Pad)') // Button label
                            .setEmoji('📱')
                            .setStyle(ButtonStyle.Success);

                        const row = new ActionRowBuilder().addComponents([dischordButton, xypadButton]);

                        return res.send({
                            type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
                            data: {
                                components: [row.toJSON()],
                                content: 'Make sure you are watching the stream on the voice chat , and we will assign you with a role to perform with.'
                            },
                        });
                    } else {
                        return res.send({
                            type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
                            data: {
                                content: 'We\'re currently offline or the server is resetting!. Try again soon!'
                            },
                        });
                    }

                case 'reroll':
                    //temp message.
                    return res.send({
                        type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
                        data: {
                            content: 'Admin command to reroll.'
                        }
                    });
                case 'command':
                    //temp message.
                    return res.send({
                        type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
                        data: {
                            content: 'Admin command to send a command.'
                        }
                    });
                case 'next_part_bougus':
                    //temp mesage. 
                    //leaving off to test the default command. 
                    return res.send({
                        type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
                        data: {
                            content: 'Admin command to go to the next part.'
                        }
                    });
                case 'glitch':
                    //temp message. 
                    return res.send({
                        type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
                        data: {
                            content: 'sending a glitch.'
                        }
                    });
                default:
                    //we don't recognize that command. 
                    return res.send({
                        type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
                        data: {
                            content: 'I don\'t recognize that command. Try again later!'
                        }
                    });
            }
        }

        if (type === InteractionType.MESSAGE_COMPONENT) {
            const componentId = data.custom_id;
            let button = state.button_dictionary[componentId]; //the button "state" object, representing the behavior
            let type;
            if (button && button.type) {
                type = button.type;
            } else {
                // this isn't in the dictionary. 
                // there is meta information after the pipe of the intro and 
                const [customAction, socketEvent] = componentId.split('|');
                //this will produce  meta data on the button after a pipe that will be a custom action. 

                switch (customAction) {
                    case 'introduction':
                    case 'reroll': //not setup might store messge state to allow cancel. right now reroll button calls introduction. 
                    case 'xypad':
                        button = {};
                        type = customAction; //important part. - setting the path for the switch statement in the next block.
                        if (socketEvent) {
                            //todo - make re-usable function. 
                            state.io.to(state.current_presenter).emit(
                                "discord_trigger",
                                {
                                    eventName: socketEvent,
                                    avatar: avatarURL,
                                    user: username
                                });
                        }
                        break;
                    default:
                        console.log("No type found for customAction from customID: " + customAction + " full: " + componentId);
                }
            }

            if (!button) {
                //TODO - also handle logic of user that created this message not being the person that is clicking it. 
                //could not find the button.
                //for now attempt to go down the "reroll" path. 
                type = 'introduction';
            }

            let nestedButton;
            let row, row2, row3, rowButtons, row2Buttons, row3Buttons, rowButton, buttonStyle, buttonDisplay, components;
            let rows = [];
            const endEventName = button ? button.endEventName : "";
            let commonRow = commonButtonsRow(endEventName);
            let i; //re-using for for-loops 

            //TODO - add re-roll here. 
            if (!state.presenter_connected) {
                return res.send({
                    type: InteractionResponseType.UPDATE_MESSAGE,
                    data: {
                        content: 'We\'re currently offline or the server is resetting!. Try again in a second!',
                        components: []
                    },
                });
            };

            switch (type) {
                case 'introduction':
                    const nextRoleIndex = getNextChatRoleIndex(state);
                    const newRole = state.chatRoles[nextRoleIndex];
                    row = new ActionRowBuilder();
                    rowButtons = [];
                    if (newRole && newRole.triggers) {
                        for (i = 0; i < newRole.triggers.length; i++) {
                            nestedButton = newRole.triggers[i]; //TODO - should we pull from dictionary ? 
                            type = nestedButton.type || 'trigger';
                            rowButton = new ButtonBuilder()
                                .setCustomId(nestedButton.id) //do we need a way to pull the id from the dictionary ? 
                                .setLabel(nestedButton.display)
                                .setStyle(getButtonStyle(nestedButton.style)); //todo - setup style.
                            if (nestedButton.emoji) {
                                rowButton.setEmoji(nestedButton.emoji);
                            }
                            rowButtons.push(rowButton);
                        }

                        row.addComponents(rowButtons);
                        rows.push(row.toJSON());
                        rows.push(commonRow);

                        const description = newRole.description || "Interact with the stream.";

                        return res.send({
                            type: InteractionResponseType.UPDATE_MESSAGE,
                            data: {
                                content: description,
                                flags: newRole.ephemeral ? 64 : undefined, //attempting this since the ephermeral doesn't seem to work otherwise.
                                components: rows
                            },
                        });
                    } else {
                        return res.send({
                            type: InteractionResponseType.UPDATE_MESSAGE,
                            data: {
                                content: "Sorry! There are no roles left for this part. Try again later!",
                                components: []
                            },
                        });
                    }
                case 'xypad':
                    // this creates a QR code that sends user information to the xy pad.
                    // this might be a memory heavy operation.
                    const xyPadLink = "https://experiments.thornebrandt.com/dischord/?user=" + username + "&u=" + userID + "&a=" + avatarID; //shortened to make smaller. 
                    const linkString = `[XY Pad](${xyPadLink})`;

                    try {

                        // Define __dirname for ESM
                        const __filename = fileURLToPath(import.meta.url);
                        const __dirname = path.dirname(__filename);

                        const filename = `qr_${username}_${user.id}.png`;
                        const filePath = path.join(__dirname, '../public/dischord/qr', filename);
                        await QRCode.toFile(filePath, xyPadLink, {
                            errorCorrectionLevel: 'H',
                            width: 500
                        }, (err) => {
                            if (err) {
                                console.error("Failed to generate QR code: ", err);
                            }
                        });

                        const qrCodeUrl = `https://experiments.thornebrandt.com/dischord/qr/${filename}`;

                        return res.send({
                            type: InteractionResponseType.UPDATE_MESSAGE,
                            data: {
                                ephemeral: true,
                                flags: InteractionResponseFlags.EPHEMERAL,
                                content: "Here is your XY Pad: " + linkString + "\n" + qrCodeUrl,
                                components: []
                            },
                        });

                    } catch (error) {
                        console.error("Failed to generate QR code: ", error);
                        return res.send({
                            type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
                            data: {
                                ephemeral: true,
                                flags: InteractionResponseFlags.EPHEMERAL,
                                content: 'Failed to generate QR code, but here is your personalized link: \n' + linkString,
                                components: []
                            },
                        });
                    }
                    break; //just in case. 
                case 'ui':
                    // the button clicked was meant to be discord UI. 
                    // display the next nexted loop of buttons within this role's trigger index. 
                    // it possibly sends an optional socket event.  In case we need to create an object and then manipulate for next set. 
                    //this one also creates a second row. 

                    button = state.button_dictionary[componentId]; //here for readability. 
                    if (!button) {
                        //TODO: could possibly be a different part so make sure to update the messaging. 
                        return res.send({
                            type: InteractionResponseType.UPDATE_MESSAGE,
                            data: {
                                content: "Sorry! We could not find a button associated with " + componentId,
                                components: []
                            },
                        });
                    }

                    if (button && button.triggers) {
                        row = new ActionRowBuilder();
                        rowButtons = [];
                        row2 = null;
                        row2Buttons = null;
                        row3 = null;
                        row3Buttons = null;

                        for (i = 0; i < button.triggers.length; i++) {
                            nestedButton = button.triggers[i];
                            buttonStyle = getButtonStyle(nestedButton.style);
                            buttonDisplay = nestedButton.display || nestedButton.id;
                            if (nestedButton.id) {
                                rowButton = new ButtonBuilder()
                                    .setCustomId(nestedButton.id)
                                    .setLabel(buttonDisplay)
                                    .setStyle(buttonStyle); //todo - setup style.
                                if (nestedButton.emoji) rowButton.setEmoji(nestedButton.emoji);

                                //this is a little WET due to timecrunch - will refactor later.
                                if (i < 5) {
                                    rowButtons.push(rowButton);
                                } else if (i < 10) {
                                    if (!row2) {
                                        row2 = new ActionRowBuilder();
                                        row2Buttons = [];
                                    }
                                    row2Buttons.push(rowButton);
                                } else {
                                    if (!row3) {
                                        row3 = new ActionRowBuilder();
                                        row3Buttons = [];
                                    }
                                    row3Buttons.push(rowButton);
                                }

                            } else {
                                console.log("No id found for button: " + button.id + " button " + i);
                            }
                        }

                        //send eventName if it exists. 
                        if (button.eventName) {
                            state.io.to(state.current_presenter).emit(
                                "discord_trigger",
                                {
                                    eventName: button.eventName,
                                    avatar: avatarURL,
                                    user: username
                                }
                            );
                        }

                        let response = button.response || "Sent! Check the video stream!";
                        let flags = button.ephemeral ? 64 : 0; //attempting this since the ephermeral doesn't seem to work otherwise.


                        row.addComponents(rowButtons);
                        rows.push(row.toJSON());
                        //again - wet code due to timecrunch. 
                        if (row2) {
                            row2.addComponents(row2Buttons);
                            rows.push(row2.toJSON());
                        }
                        if (row3) {
                            row3.addComponents(row3Buttons);
                            rows.push(row3.toJSON());
                        }
                        rows.push(commonRow);

                        return res.send({
                            type: InteractionResponseType.UPDATE_MESSAGE,
                            data: {
                                content: response,
                                flags: flags,
                                components: rows
                            },
                        });
                    }
                    break;
                case 'trigger':
                    button = state.button_dictionary[componentId]; //repeated here for readability. 

                    let components = [];
                    if (!button.hide && message && message.components) {
                        // Clone the existing components to avoid mutation issues
                        components = message.components.map(actionRow => {
                            return ActionRowBuilder.from(actionRow);
                        });
                    }

                    let response = button.response || "Sent! Check the video stream!";

                    //sending socket event. 
                    state.io.to(state.current_presenter).emit(
                        "discord_trigger",
                        {
                            eventName: button.eventName,
                            avatar: avatarURL,
                            user: username
                        });

                    //displaying the response. ( components are if they stayed put. )
                    let flags = button.ephemeral ? 64 : 0; //attempting this since the ephermeral doesn't seem to work otherwise.

                    return res.send({
                        type: InteractionResponseType.UPDATE_MESSAGE,
                        data: {
                            ephemeral: button.ephemeral,
                            flags: flags,
                            content: response,
                            components: components
                        },
                    });
                default:
                    return res.send({
                        type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
                        data: {
                            content: 'I don\'t recognize the command type: ' + type + ' Try again later!'
                        }
                    });
            }
        }

    });
}