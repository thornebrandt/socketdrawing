// This file is used to install the commands to the discord server.
// This can be ran locally. It will install the commands to the discord server.

import config from '../config.js';

import { InstallGlobalCommands } from './utils.mjs';

const COMMANDS = [
    {
        name: 'dischord',
        description: 'Start interacting.',
    },
    {
        name: 'reroll',
        description: 'Request a new role.'
    },
    {
        name: 'command',
        description: 'A modal to send a special command.'
    },
    {
        name: 'next_part',
        description: 'Request the next part.'
    },
    {
        name: 'glitch',
        description: 'Cause a glitch.'
    }
];

InstallGlobalCommands(config.discord_app_id, COMMANDS);



