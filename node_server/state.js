const state = {
    presenter_connected: false,
    current_presenter: null,
    discord_connected: false,
    discord_client: null,
    discordChannel: null,
    clients: [],
    presenters: [],
    chatRoles: [], //nested chat roles coming from the presenter
    button_dictionary: {}, //flatted button dictionary from nested chat roles
    current_chat_role_index: 0,
};

module.exports = state;